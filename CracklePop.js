let checks = [
    [3, "Crackle"],
    [5, "Pop"]
];

for(let i = 1; i <= 100; i++){
    let print = "";
    checks.forEach(check => {
        if(i % check[0] === 0){
            print += check[1];
        };
    });
    console.log(print.length > 0 ? print : i);
};